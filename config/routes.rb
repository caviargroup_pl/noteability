Noteability::Engine.routes.draw do

  root 'notes#index'

  resources :notes do
    resources :documents
  end

end
