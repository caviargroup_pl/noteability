module Noteability
  class Engine < ::Rails::Engine
    isolate_namespace Noteability
  end
end
