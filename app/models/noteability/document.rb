module Noteability
  class Document < ActiveRecord::Base
    has_attached_file :file
    do_not_validate_attachment_file_type :file

    def file_type
      case  
        when file.content_type == 'application/zip'
          'archive'
        when file.content_type == 'application/pdf'
          'pdf'
        when file.content_type.match('image')
          'image'
        when file.content_type.match('audio')
          'audio'
        when file.content_type.match('text')
          'text'
        when file.content_type.match('video')
          'video'
        when file.content_type.match('sheet')
          'excel'
        when file.content_type.match('document')
          'word'
        when file.content_type.match('slideshow')
          'powerpoint'
        else
          'file'
      end
    end



    def icon

      case  
        when file.content_type == 'application/zip'
          'fa-file-archive-o'
        when file.content_type == 'application/pdf'
          'fa-file-pdf-o'
        when file.content_type.match('image')
          'fa-file-image-o'
        when file.content_type.match('audio')
          'fa-file-audio-o'
        when file.content_type.match('text')
          'fa-file-text'
        when file.content_type.match('video')
          'fa-file-video-o'
        when file.content_type.match('sheet')
          'fa-file-excel-o'
        when file.content_type.match('document')
          'fa-file-word-o'
        when file.content_type.match('slideshow')
          'fa-file-powerpoint-o'
        else
          'fa-file'
      end
    end
  end
end
