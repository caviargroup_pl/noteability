module Noteability
  class Note < ActiveRecord::Base
    
    belongs_to :noteable, polymorphic: true
    has_many :documents

    belongs_to :owner, polymorphic: true

    def self.find_noteabled(params)
      where(noteable_id: params['id'], noteable_type: params['class'])
    end

    def add_document(attachment)
      documents.create(attachment)
    end

  end
end
