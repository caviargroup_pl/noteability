module Noteability
  class DocumentsController < ApplicationController

    def create
      Rails.logger.info current_user
      document = find_note.documents.create(document_params)
      render json: document.to_json(methods: :icon)
    end

    def destroy
      document = Noteability::Document.find(params[:id])
      document.destroy
      render json: {status: 'OK'}
    end

    private

    def document_params
      params[:document].permit!
    end

    def find_note
      Noteability::Note.find(params[:note_id])
    end

  end
end