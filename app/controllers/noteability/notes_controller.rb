module Noteability
  class NotesController < ApplicationController
    
    def index
      notes = Note.find_noteabled(JSON.parse(params[:noteable]))
      # notes = Noteability::Note.all
      render json: notes.to_json(include: {documents: {methods: [:file_type, :icon]}})
    end

    def show
      note = set_note
      render json: note.to_json(include: {documents: {methods: [:file_type, :icon]}})
    end

    def create
      if params[:noteable][:class] == 'Offer'
        noteable = Offer.find(params[:noteable][:id])
      end
      note = Noteability::Note.create(note_params)
      note.update(noteable: noteable)
      render json: note
    end

    def update
      note = set_note
      note.update(note_params)
      render json: note
    end

    def destroy
      note = set_note
      note.destroy
      render json: note
    end

    private

    def set_note
      Noteability::Note.find(params[:id])
    end

    def note_params
      params[:note].delete(:documents)
      params[:note].permit!
    end

  end
end