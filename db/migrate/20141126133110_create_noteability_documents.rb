class CreateNoteabilityDocuments < ActiveRecord::Migration
  def change
    create_table :noteability_documents do |t|
      t.references :note
      t.timestamps
    end
    add_attachment :noteability_documents, :file
  end
end
