class CreateNoteabilityNotes < ActiveRecord::Migration
  def change
    create_table :noteability_notes do |t|
      
      t.references  :owner, polymorphic: true
      t.references  :noteable, polymorphic: true
      t.string      :title
      t.text        :text

      t.boolean     :private, default: false
      
      t.timestamps
    end
  end
end
