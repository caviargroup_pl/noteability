$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "noteability/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "noteability"
  s.version     = Noteability::VERSION
  s.authors     = ["Boguslaw Tolarz"]
  s.email       = ["btolarz@gmail.com"]
  s.homepage    = "http://github.com/btolarz"
  s.summary     = "Notes gem"
  s.description = "Notes gem"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.5"
  s.add_dependency "paperclip", "~> 4.1"

  s.add_development_dependency "mysql2"
end
